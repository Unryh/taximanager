from django.contrib import admin

from taximanager.models.cost_model import Cost
from taximanager.models.driver_model import Driver
from taximanager.models.order_model import Order

from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User


# Define an inline admin descriptor for Driver model
# which acts a bit like a singleton
class DriverInline(admin.StackedInline):
    model = Driver
    can_delete = False
    verbose_name_plural = 'drivers'


# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (DriverInline, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

admin.site.register(Cost)
admin.site.register(Driver)
admin.site.register(Order)
