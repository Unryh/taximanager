from django import forms

from taximanager.models.order_model import Order


class OrderForm(forms.ModelForm):
    """Class create form for new order."""

    class Meta:
        model = Order
        fields = ('pickup_address', 'destination_address', 'date', )
        widgets = {
            'pickup_address': forms.TextInput(attrs={
                'placeholder': 'Pickup Address',
                'class': 'form-control top controls',
                'id': 'origin-input'}),
            'destination_address': forms.TextInput(attrs={
                'placeholder': 'Destination Address',
                'class': 'form-control top controls',
                'id': 'destination-input'}),
            'date': forms.DateInput(attrs={
                'placeholder': 'Date',
                'class': 'form-control date agileits w3layouts datepicker'}),
        }
