import datetime as dt

import django.db.models as models
from django.db.models import Q

from etc.application_settings import DAY_TARIFF_TIME
from etc.application_settings import NIGHT_TARIFF_TIME


class CostManager(models.Manager):
    """Manger for cost model"""

    def get_tariff(self):
        """The Method gets from DB current tariff of trip."""
        date = dt.datetime.now()
        current_hour = date.timetuple().tm_hour
        if current_hour >= DAY_TARIFF_TIME \
                and current_hour < NIGHT_TARIFF_TIME:
            tariff_type = 'd'
        else:
            tariff_type = 'n'
        tariff = super(CostManager, self).get_queryset().filter(
            Q(tariff_type=tariff_type)
            & Q(start_date__lte=date.date())).latest('id')
        return tariff
