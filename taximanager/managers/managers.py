import django.db.models as models

import googlemaps
import smtplib
from email.mime.text import MIMEText

from etc.application_settings import COMPANY_MAIL
from etc.application_settings import COMPANY_PASSWORD
from etc.application_settings import API_KEY


class OrderManager(models.Manager):
    """Manger for order model"""

    def send_email(self, mail, text):
        smtp_obj = smtplib.SMTP_SSL('smtp.googlemail.com', 465)
        smtp_obj.login(COMPANY_MAIL, COMPANY_PASSWORD)
        msg = MIMEText(text.encode('utf-8'), _charset='utf-8')
        smtp_obj.sendmail(COMPANY_MAIL, mail, msg.as_string())
        smtp_obj.quit()

    def get_distance(self, origin, destination):
        gmaps = googlemaps.Client(key=API_KEY)
        my_distance = gmaps.distance_matrix(origin, destination)
        try:
            distance = \
                my_distance['rows'][0]['elements'][0]['distance']['value']
        except KeyError:
            distance = 0
            return distance
        else:
            return distance
