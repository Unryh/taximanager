from django.db import models

from taximanager.managers.cost_manager import CostManager


class Cost(models.Model):
    """Create table with cost of trip by period."""
    NIGHT = 'n'
    DAY = 'd'
    TARIFF_TYPE_CHOICES = ((NIGHT, 'night tariff'), (DAY, 'day tariff'))
    base_cost = models.FloatField()
    start_date = models.DateField()
    cost_per_1km = models.FloatField()
    tariff_type = models.CharField(max_length=1,
                                   choices=TARIFF_TYPE_CHOICES,
                                   default=DAY)
    objects = models.Manager()
    tariff = CostManager()

    class Meta:
        ordering = ['start_date']
        verbose_name = 'cost'
        verbose_name_plural = 'costs'

    def __str__(self):
        return '{start_date} - {base_cost} - {tariff_type}'.format(
            start_date=self.start_date,
            base_cost=self.base_cost,
            tariff_type=self.tariff_type
        )
