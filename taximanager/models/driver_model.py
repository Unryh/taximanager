from django.db import models
from django.contrib.auth.models import User

from taximanager.validations.driver_validation import validate_number_of_car


class Driver(models.Model):
    """Create table with addition information about driver."""
    user = models.OneToOneField(User, on_delete=models.PROTECT)
    number_of_car = models.CharField(
        max_length=8,
        validators=[validate_number_of_car])
    color_of_car = models.CharField(max_length=20)
    car_brand = models.CharField(max_length=20)

    class Meta:
        ordering = ['number_of_car']
        verbose_name = 'driver'
        verbose_name_plural = 'drivers'

    def __str__(self):
        return self.number_of_car
