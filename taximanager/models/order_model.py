from datetime import date

from django.db import models
from django.contrib import auth

from taximanager.models.driver_model import Driver


class Order(models.Model):
    """The class creates table for client orders."""

    STATUS_CHOICES = (('False', 'accepted'), ('True', 'free'))
    client = models.ForeignKey(auth.models.User, on_delete=models.PROTECT)
    pickup_address = models.CharField(max_length=200)
    destination_address = models.CharField(max_length=200)
    trip_distance = models.IntegerField(null=True)
    trip_cost = models.IntegerField(null=True)
    date = models.DateField(default=date.today())
    driver = models.ForeignKey(Driver, to_field='user_id',
                               on_delete=models.PROTECT,
                               blank=True, null=True)
    status = models.CharField(max_length=5, choices=STATUS_CHOICES,
                              default=True)

    def __str__(self):
        return self.pickup_address + ' - ' + self.destination_address
