function show()
        {
        var id = $('#driver_id').val();
        var page_numb = $('#page_numb').val();

            $.ajax({
                type: 'get',
                dateType: 'json',
                url: "/page_refresh/" + id + "/" + page_numb,
                cache: false,
                success: function(data){
                    $("#list").html(data.html_order);
                }
            });
        }
        $(document).ready(function(){
            show();
            setInterval(show,30000);
        });