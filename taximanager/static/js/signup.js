$(function () {
    $("#id_username").change(function () {
      console.log( $(this).val() );
      var username = $(this).val();
      var form = $(this).closest("form");
        $.ajax({
        url: form.attr("validate-user-url"),
        data: {
          'username': username
        },
        dataType: 'json',
        success: function (data) {
          if (data.exists) {
            alert(data.error);
          }
        }
      });

    });
});