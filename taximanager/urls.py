"""taximanager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.views.generic import TemplateView
from django.contrib import admin
from django.contrib.auth import views as auth_views

from taximanager.views.order_view import OrderCreateView
from taximanager.views import signup_view
from taximanager.views import home_view
from taximanager.views import validate_view
from taximanager.views.page_refreshing_view import page_refreshing_view
from taximanager.views.list_of_orders_view import AcceptOrderView
from taximanager.views.list_of_orders_view import ListOfOrdersView


urlpatterns = [
    url(r'^$', home_view.home, name='home'),

    url(r'^role/$',
        TemplateView.as_view(template_name='registration/role.html'),
        name='role'),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$',
        auth_views.logout,
        {'next_page': 'login'},
        name='logout'),
    url(r'^user_signup/$', signup_view.user_signup, name='user_signup'),
    url(r'^signup/$', signup_view.signup, name='signup'),

    url(r'^validate_user/$',
        validate_view.validate_user,
        name='validate_user'),
    url(r'^validate_user_login/$',
        validate_view.validate_user_login,
        name='validate_user_login'),

    url(r'^order/$', OrderCreateView.as_view(), name='order_form'),
    url(r'^list_of_orders/(?P<driver_id>[0-9]+)/$',
        ListOfOrdersView.as_view(),
        name='list_of_orders'),
    url(r'^accept_order/(?P<driver_id>[0-9]+)/(?P<order_id>[0-9]+)/$',
        AcceptOrderView.as_view(),
        name='accept_order'),
    url(r'^page_refresh/(?P<driver_id>[0-9]+)/(?P<page_numb>[0-9]+)/$',
        page_refreshing_view,
        name='page_refresh'),

    url(r'^admin/', admin.site.urls),
]
