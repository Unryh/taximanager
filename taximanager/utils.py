def create_message(pickup_address, destination_address, trip_cost):
    text = 'Your order\nfrom - {0}\nto - {1}\ncost - {2} UAH'.format(
        pickup_address,
        destination_address,
        trip_cost
    )
    return text


def create_message_after_accepted(order):
    text = 'Your order\nfrom - {0}\nto - {1}\ncost - {2} UAH, was accepted.\n'\
           'Auto: {3}, num. {4},\ncolor: {5}\nname of driver: {6}'\
        .format(
                order.pickup_address,
                order.destination_address,
                order.trip_cost,
                order.driver.car_brand,
                order.driver.number_of_car,
                order.driver.color_of_car,
                order.driver.user.first_name
                )
    return text
