import re
from django.core.exceptions import ValidationError


def validate_number_of_car(value):
    pattern = r'[\w\d][\w\d\-]{6}[\w]'
    if not re.match(pattern, value):
        raise ValidationError(u"The number of car isn't correct."
                              u"It can contain numbers, letter or dash."
                              u"(example: AE1234AA  or  123-45AE)")
