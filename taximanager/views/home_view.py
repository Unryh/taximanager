from django.shortcuts import render


def home(request):
    if hasattr(request.user, 'driver'):
        return render(request, 'home.html', {'data': 'Available Orders',
                                             'url': 'list_of_orders/' + str(request.user.id),
                                             'data_home': 'Go to available orders'})
    return render(request, 'home.html', {'url': 'order', 'data': 'Booking Form',
                                         'data_home': 'Order your trip'})
