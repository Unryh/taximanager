import django.views.generic as generic
import django.urls as urls
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect

from taximanager.utils import create_message_after_accepted
from taximanager.models.order_model import Order
from taximanager.managers.managers import OrderManager


class ListOfOrdersView(generic.TemplateView):
    template_name = 'list_of_orders.html'

    def get(self, request, driver_id):
        order_list = Order.objects.filter(status=True)
        paginator = Paginator(order_list, 5)
        page_all = request.GET.get('page_all')
        try:
            orders = paginator.page(page_all)
        except PageNotAnInteger:
            orders = paginator.page(1)
        except EmptyPage:
            orders = paginator.page(paginator.num_pages)
        driver_list = Order.objects.filter(
            driver_id=driver_id).order_by('id').reverse()[:10]
        paginator_driver = Paginator(driver_list, 10)
        page_driver = request.GET.get('page_driver')
        try:
            orders_driver = paginator_driver.page(page_driver)
        except PageNotAnInteger:
            orders_driver = paginator_driver.page(1)
        except EmptyPage:
            orders_driver = paginator_driver.page(paginator_driver.num_pages)
        return render(request, self.template_name,
                      {'all_orders': orders,
                       'driver_orders': orders_driver,
                       'driver_number': driver_id})


class AcceptOrderView(generic.TemplateView):
    template_name = 'list_of_orders.html'

    def get(self, request, driver_id, order_id):
        Order.objects.filter(id=order_id).update(
            driver_id=driver_id)
        order = Order.objects.select_related().filter(
            id=order_id, driver_id=driver_id)
        text = create_message_after_accepted(order[0])
        email = order[0].client.email
        OrderManager().send_email(email, text)
        Order.objects.filter(id=order_id).update(
            status=False)
        return HttpResponseRedirect(urls.reverse_lazy('list_of_orders',
                                                      args=[driver_id]))
