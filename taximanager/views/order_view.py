import math

import django.urls as urls
import django.views.generic as generic
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

import taximanager.forms.order_form as forms
from taximanager.managers.managers import OrderManager
from taximanager.models import cost_model
from taximanager.utils import create_message


@method_decorator(login_required(login_url='login'), name='dispatch')
class OrderCreateView(generic.CreateView):
    """Create new order in DB."""
    form_class = forms.OrderForm
    template_name = 'order_form.html'
    http_method_names = ('get', 'post')
    success_url = urls.reverse_lazy('home')

    def form_valid(self, form):
        """The Method calculates price and sends to email data of trip
        to client."""
        order = form.save(commit=False)
        order.client = self.request.user
        tariff = cost_model.Cost.tariff.get_tariff()
        trip_distance = (OrderManager().get_distance(
            order.pickup_address, order.destination_address))/1000
        if trip_distance != 0:
            order.trip_cost = math.ceil(
                tariff.base_cost + trip_distance * tariff.cost_per_1km)
            order.trip_distance = trip_distance
            text = create_message(order.pickup_address, order.destination_address,
                                  order.trip_cost)
            email = order.client.email
            OrderManager().send_email(email, text)
            order.save()
            return redirect(self.success_url)
        else:
            return redirect('order_form')