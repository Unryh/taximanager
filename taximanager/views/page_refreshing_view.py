from django.http import JsonResponse
from django.template.loader import render_to_string
import taximanager.models.order_model as models
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def page_refreshing_view(request, driver_id, page_numb):
    if request.method == 'GET':
        order_list = \
            models.Order.objects.filter(status=True).order_by('id').reverse()
        paginator = Paginator(order_list, 5)
        page_all = page_numb
        try:
            orders = paginator.page(page_all)
        except PageNotAnInteger:
            orders = paginator.page(1)
        except EmptyPage:
            orders = paginator.page(paginator.num_pages)
        data = dict()
        data['html_order'] = render_to_string(
            'includes/page_refreshing.html',
            {'all_orders': orders,
             'driver_number': driver_id
             }
        )
        return JsonResponse(data)
