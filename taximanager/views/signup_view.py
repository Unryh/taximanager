from django.shortcuts import render
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.db import transaction
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from taximanager.forms.signup_form import SignUpForm, DriverForm
from taximanager.models.driver_model import Driver


@transaction.atomic
def signup(request):

    @receiver(post_save, sender=User)
    def update_user_driver(sender, instance, created, **kwargs):
        if created:
            Driver.objects.create(user=instance)
        instance.driver.save()

    if request.method == 'POST':
        user_form = SignUpForm(request.POST)
        driver_form = DriverForm(request.POST)
        if user_form.is_valid() and driver_form.is_valid():
            user = user_form.save()
            user.refresh_from_db()  # This will load Driver created by the Signal
            driver_form = DriverForm(request.POST, instance=user.driver)
            driver_form.full_clean()
            driver_form.save()
            raw_password = user_form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        user_form = SignUpForm()
        driver_form = DriverForm()
    return render(request, 'registration/sign_up.html', {
        'form': user_form,
        'driver_form': driver_form
})


def user_signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'registration/user_signup.html', {'form': form})