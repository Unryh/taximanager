from django.contrib.auth.models import User
from django.http import JsonResponse


def validate_user(request):
    username = request.GET.get('username', None)
    data = {
        'exists': User.objects.filter(username__iexact=username).exists()
    }
    if data['exists']:
        data['error'] = 'A user with this username already exists!'
    return JsonResponse(data)


def validate_user_login(request):
    username = request.GET.get('username', None)
    data = {
        'exists': not (User.objects.filter(username__iexact=username).exists())
    }
    if data['exists']:
        data['error'] = 'The user does not exists!'
    return JsonResponse(data)